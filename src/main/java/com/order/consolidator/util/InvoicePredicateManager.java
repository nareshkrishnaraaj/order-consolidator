package com.order.consolidator.util;

import java.math.BigDecimal;
import java.util.function.Predicate;

import org.springframework.util.StringUtils;

import com.order.consolidator.entities.TXML.Message.Order.Invoices.InvoiceDetail.InvoiceLines.InvoiceLineDetail.DiscountDetails.DiscountDetail;
import com.order.consolidator.entities.TXML.Message.Order.Invoices.InvoiceDetail.InvoiceLines.InvoiceLineDetail.TaxDetails.TaxDetail;

public class InvoicePredicateManager {
	
	
	public static Predicate<TaxDetail> getInvoiceTaxChargeByCategory(
			String categoryId) {

		return c -> constructInvoiceTaxCategoryId(c).equals(categoryId);
	}
	
	
	/**
	 * To identify each Tax Category uniquely, tax categoryId is defined as "Tax
	 * Category~Charge Category~Request Type
	 * 
	 * @param taxDetail
	 * @return
	 */
	private static String constructInvoiceTaxCategoryId(TaxDetail taxDetail) {
		StringBuilder categoryId = new StringBuilder();
		categoryId.append(taxDetail.getTaxCategory()).append("~");
		categoryId.append(taxDetail.getChargeCategory().getValue()).append("~");
		categoryId.append(taxDetail.getRequestType());
		return categoryId.toString();
	}

	/**
	 * Checks if the discount is of type Coupon with the couponId provided.
	 * 
	 * @param coupon
	 * @return
	 */
	public static Predicate<DiscountDetail> getCouponFilter(Object coupon) {
		return d -> d.getDiscountType().equals("Coupon") && d.getCouponId().getValue().equals(String.valueOf(coupon));
	}

	/**
	 * Checks if the Discount is of Type Coupon
	 * 
	 * @return
	 */
	public static Predicate<DiscountDetail> getCouponFilter() {
		return d -> d.getDiscountType().equals("Coupon");
	}

	/**
	 * Checks if the Discount is of type Promotion
	 * 
	 * @return
	 */
	public static Predicate<DiscountDetail> getPromotionFilter() {
		return d -> d.getDiscountType().equals("Promotion");
	}

	/**
	 * Checks if the Discount if of type Promotion having ExtDiscountId same as the
	 * provided discountId
	 * 
	 * @param discountId
	 * @return
	 */
	public static Predicate<DiscountDetail> getPromotionFilterByDiscId(Object discountId) {

		return d -> !StringUtils.isEmpty(d.getExtDiscountId())
				&& d.getExtDiscountId().equals(String.valueOf(discountId));
	}

	/**
	 * Checks if the Discount if of type Promotion having DiscountPercentage same as
	 * the provided discountId
	 * 
	 * @param discountId
	 * @return
	 */
	public static Predicate<DiscountDetail> getPromotionFilterByDiscPercent(Object discountId) {

		return d -> !StringUtils.isEmpty(d.getDiscountPercentage())
				&& !StringUtils.isEmpty(d.getDiscountPercentage().getValue())
				&& d.getDiscountPercentage().getValue().toPlainString().equals(discountId);
	}

	/**
	 * Checks if the Discount if of type Promotion having DiscountDescription same
	 * as the provided discountId
	 * 
	 * @param discountId
	 * @return
	 */
	public static Predicate<DiscountDetail> getPromotionFilterByDesc(Object discountId) {

		return d -> !StringUtils.isEmpty(d.getDescription()) && !StringUtils.isEmpty(d.getDescription().getValue())
				&& d.getDescription().getValue().equals(String.valueOf(discountId));
	}

	/**
	 * Checks if the Discount if of type BRD
	 * 
	 * @return
	 */
	public static Predicate<DiscountDetail> getBRDFilter() {
		return d -> d.getDiscountType().equals("BRD");
	}
	
	
	public static Predicate<DiscountDetail> getBRDFilterById(String brdId) {
		return d -> d.getDiscountType().equals("BRD") && d.getCouponId().getValue().equals(brdId) ;
	}

	/**
	 * Checks if the Discount if of type Employee
	 * 
	 * @return
	 */
	public static Predicate<DiscountDetail> getEmployeeDiscountFilter() {
		return d -> d.getDiscountType().equals("Employee");
	}

	/**
	 * If the provided percentage is zero, checks if the Discount if of type
	 * Employee(regular discount). If the provided percentage is non-zero value,
	 * checks if the Discount if of type Employee including the discount percentage.
	 * 
	 * @param percentage
	 * @return
	 */
	public static Predicate<DiscountDetail> getEmployeeDiscountFilterById(Object percentage) {

		// For single employee discounts there is no percentage
		if (((BigDecimal) percentage).equals(BigDecimal.ZERO))
			return getEmployeeDiscountFilter();

		return getEmployeeDiscountFilter()
				.and(d -> d.getDiscountPercentage().getValue().equals((BigDecimal) percentage));
	}
}
