package com.order.consolidator.services;

import java.util.List;

/**
 * Service invoked to initiate the Consolidation for OrderLine level. The
 * Collection of items that needs to be consolidated should of same type.
 * 
 * @author Ismail
 *
 * @param <T>
 */
public interface OrderLineConsolidation<T> {

	T consolidate(List<T> list);
}
