package com.order.consolidator.services;

/**
 * Service invoked to initiate the Order Consolidation Process.
 * @author Ismail
 *
 * @param <T>
 */
public interface OrderConsolidation<T> {

	T consolidate(T t);
}
