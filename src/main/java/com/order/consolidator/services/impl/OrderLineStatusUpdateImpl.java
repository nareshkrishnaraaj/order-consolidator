package com.order.consolidator.services.impl;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import com.order.consolidator.entities.TXML.Message.Order.OrderLines.OrderLine;
import com.order.consolidator.entities.TXML.Message.Order.OrderLines.OrderLine.SplitLines;
import com.order.consolidator.entities.TXML.Message.Order.OrderLines.OrderLine.SplitLines.SplitLine;
import com.order.consolidator.services.OrderLineStatusUpdate;

/**
 * Defines and applies the OrderLine status based on the pre-defined set of
 * rules.
 * 
 * @author Ismail
 *
 */
@Service
public class OrderLineStatusUpdateImpl implements OrderLineStatusUpdate<OrderLine> {

	private static final Logger LOGGER = LogManager.getLogger(OrderLineStatusUpdateImpl.class);

	@Override
	public List<OrderLine> consolidate(List<OrderLine> orderLines) {
		LOGGER.info("Applying OrderLine Status update Rules...");
		Optional<OrderLine> firstLine = orderLines.stream().findFirst();
		if (firstLine.isPresent()) {
			LOGGER.info("Triggering rules for {}", firstLine.get().getLineNumber());
			applyRules(orderLines, firstLine.get());
		}
		LOGGER.info("Updated the OrderLines status based on the rules.");
		return orderLines;
	}

	/**
	 * The orderlines are run through the pre-defined set of rules to identify the
	 * OrderLine Status
	 * 
	 * @param orderLines
	 * @param firstOrderLine
	 * @param rule
	 */
	protected void applyRules(List<OrderLine> orderLines, OrderLine firstOrderLine) {

		//update the isUpdated flag to true, if any of the lineItems, true --> this flag triggers the shipment email.
		boolean isUpdatedFlag = orderLines.stream()
		.filter(orderLine -> !Objects.isNull(orderLine.getIsUpdated()) && !StringUtils.isEmpty(orderLine.getIsUpdated().getValue()))
		.anyMatch(orderLine -> orderLine.getIsUpdated().getValue().equals("true"));
		if(isUpdatedFlag)
			firstOrderLine.getIsUpdated().setValue(String.valueOf(isUpdatedFlag));
		
		//OrderLines with status BackOrdered have a special consideration for SplitOrderLine status.
		//Those orderline status are updated according to the SplitOrderLine status Rule, before the consolidated status is updated.
		applyBackOrderRule(orderLines);
		
		// start the rule execution with the highest priority status - All Cancelled items rule
		// and through the order.
		// ********DO NOT CHANGE THE BELOW ORDER, UNLESS A CHANGE IN THE PRIORITY OF
		// STATUS IS REQUIRED. IF A NEW STATUS IS REQUIRED, POSITION IT ACCORDING TO THE
		// ORDER******
		final OrderStatusRules rule = OrderStatusRules.CANCELLED;

		switch (rule) {
		case CANCELLED: 
			if (applyAllCancellationRules(orderLines, firstOrderLine))
				return;
		
		case P_SHIPPED:
		case SHIPPED:
			if (applyShippingRules(orderLines, firstOrderLine))
				return;

		case RELEASED:
		case P_RELEASED:
			if (applyReleaseRules(orderLines, firstOrderLine))
				return;

		case DC_ALLOCATED:
		case DO_CREATED:
			if (applyAllocationRules(orderLines, firstOrderLine))
				return;
			
		case BACK_ORDERED:
			if(applyBackOrderedRules(orderLines, firstOrderLine))
				return;

		case ALLOCATED:
			if (applyOtherRules(orderLines, firstOrderLine))
				return;
			break;
		}
	}
	
	/**
	 * Applies the rule to check if all the lines are cancelled
	 * @param orderLines
	 * @param firstOrderLine
	 * @return
	 */
	private Boolean applyAllCancellationRules(List<OrderLine> orderLines, OrderLine firstOrderLine) {
		//Before we check for any other Status make sure all items are not Cancelled
		
		//If all Items are cancelled set Status to Canceled
		boolean isAllCancelled = orderLines.stream().allMatch(OrderStatusRules.CANCELED.getFilter());
		if(!isAllCancelled)
			isAllCancelled = orderLines.stream().allMatch(OrderStatusRules.CANCELLED.getFilter());

		if(isAllCancelled) {
			firstOrderLine.setOrderLineStatus(OrderStatusRules.CANCELED.getStatus());
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	/**
	 * Applies the following rules, excluding the Cancelled items:
	 *  If atleast one of the OrderLine is Shipped --> Status = Partially Shipped
	 *  If all the OrderLines are Shipped ---> Status = Shipped.
	 * 
	 * @param orderLines
	 * @param firstOrderLine
	 */
	private Boolean applyShippingRules(List<OrderLine> orderLines, OrderLine firstOrderLine) {
		
		//If all Items are Shipped, except the Canceled. Set the Status as Shipped.
		boolean isFullyShipped = orderLines.stream()
				.filter(orderLine -> !StringUtils.isEmpty(orderLine.getOrderLineStatus()))
				.filter(orderLine -> !orderLine.getOrderLineStatus().equals(OrderStatusRules.CANCELLED.getStatus()))
				.filter(orderLine -> !orderLine.getOrderLineStatus().equals(OrderStatusRules.CANCELED.getStatus()))
				.allMatch(OrderStatusRules.SHIPPED.getFilter());
		

		if (isFullyShipped) {
			firstOrderLine.setOrderLineStatus(OrderStatusRules.SHIPPED.getStatus());
			return Boolean.TRUE;
		} else {
			//If it's not fully Shipped and atleast an item is Shipped, Set the Status as Partially Shipped
			boolean isShippedPartially = orderLines.stream().filter(o -> !StringUtils.isEmpty(o.getOrderLineStatus()))
					.filter(orderLine -> !orderLine.getOrderLineStatus().equals(OrderStatusRules.CANCELLED.getStatus()))
					.filter(orderLine -> !orderLine.getOrderLineStatus().equals(OrderStatusRules.CANCELED.getStatus()))
					.anyMatch(OrderStatusRules.SHIPPED.getFilter());
			if (isShippedPartially) {
				firstOrderLine.setOrderLineStatus(OrderStatusRules.P_SHIPPED.getStatus());
				return Boolean.TRUE;
			}
		}
		return Boolean.FALSE;
	}

	/**
	 * Applies the rule: If atleast one of the OrderLine is partially released,
	 * Orderline Status is Partially Shipped.
	 * 
	 * @param orderLines
	 * @param firstOrderLine
	 */
	private Boolean applyReleaseRules(List<OrderLine> orderLines, OrderLine firstOrderLine) {

		boolean isReleased = orderLines.stream()
				.filter(orderLine -> !StringUtils.isEmpty(orderLine.getOrderLineStatus()))
				.filter(orderLine -> !orderLine.getOrderLineStatus().equals(OrderStatusRules.CANCELLED.getStatus()))
				.allMatch(OrderStatusRules.RELEASED.getFilter());
		

		if (isReleased) {
			firstOrderLine.setOrderLineStatus(OrderStatusRules.RELEASED.getStatus());
			return Boolean.TRUE;
		} else {
			boolean isReleasedPartially = orderLines.stream().filter(o -> !StringUtils.isEmpty(o.getOrderLineStatus()))
					.filter(orderLine -> !orderLine.getOrderLineStatus().equals(OrderStatusRules.CANCELLED.getStatus()))
					.filter(orderLine -> !orderLine.getOrderLineStatus().equals(OrderStatusRules.CANCELED.getStatus()))
					.anyMatch(OrderStatusRules.RELEASED.getFilter());
			if (isReleasedPartially) {
				firstOrderLine.setOrderLineStatus(OrderStatusRules.P_RELEASED.getStatus());
				return Boolean.TRUE;
			}
		}
		return Boolean.FALSE;
	}

	/**
	 * Applies the rule:
	 * 
	 * @param orderLines
	 * @param firstOrderLine
	 */
	private Boolean applyAllocationRules(List<OrderLine> orderLines, OrderLine firstOrderLine) {
		boolean isDCAllocated = orderLines.stream().filter(o -> !StringUtils.isEmpty(o.getOrderLineStatus()))
				.anyMatch(OrderStatusRules.DC_ALLOCATED.getFilter());

		if (isDCAllocated) {
			firstOrderLine.setOrderLineStatus(OrderStatusRules.DC_ALLOCATED.getStatus());
			return Boolean.TRUE;
		}
		
		boolean isDOCreated = orderLines.stream().filter(o -> !StringUtils.isEmpty(o.getOrderLineStatus()))
				.anyMatch(OrderStatusRules.DO_CREATED.getFilter());
		if (isDOCreated) {
			firstOrderLine.setOrderLineStatus(OrderStatusRules.DO_CREATED.getStatus());
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
	
	
	/**
	 * Applies the rules for BackOrdered status:
	 * @param orderLines
	 * @param firstOrderLine
	 * @return
	 */
	private Boolean applyBackOrderedRules(List<OrderLine> orderLines, OrderLine firstOrderLine) {
		boolean isBackOrdered = orderLines.stream().filter(o -> !StringUtils.isEmpty(o.getOrderLineStatus()))
				.anyMatch(OrderStatusRules.BACK_ORDERED.getFilter());

		if (isBackOrdered) {
			firstOrderLine.setOrderLineStatus(OrderStatusRules.BACK_ORDERED.getStatus());
			return Boolean.TRUE;
		}		
		return Boolean.FALSE;
	}
	
	
	private void applyBackOrderRule(List<OrderLine> orderLines) {
		List<OrderLine> backOrderedOrderLines = 
				orderLines.stream().filter(o -> !StringUtils.isEmpty(o.getOrderLineStatus()))
				.filter(orderLine -> orderLine.getOrderLineStatus().equals("Back Ordered")).collect(Collectors.toList());
		backOrderedOrderLines.stream().forEach(orderLine -> consolidateSplitLineStatus(orderLine));
	}
	
	private void consolidateSplitLineStatus(OrderLine orderLine) {
		if(!ObjectUtils.isEmpty(orderLine) && !ObjectUtils.isEmpty(orderLine.getSplitLines())) {
			SplitLines splitLines = orderLine.getSplitLines();
			List<SplitLine> splitLineList = splitLines.getSplitLine();
			List<String> splitLinesStatus = 
					splitLineList.stream().map(splitLine -> splitLine.getOrderLineStatus()).collect(Collectors.toList());
			Optional<String> splitLineFinalStatusOptional = 
					splitLinesStatus.stream().reduce((status1, status2) -> applySplitLineStatusRule(status1, status2));
			if(splitLineFinalStatusOptional.isPresent()) {
				String orderLineStatus = splitLineFinalStatusOptional.get();
				orderLine.setOrderLineStatus(orderLineStatus);
			}
		}
	}
	
	/**
	 * Returns the status that has higher priority
	 * @param status1
	 * @param status2
	 * @return
	 */
	private String applySplitLineStatusRule(String status1, String status2) {

		final OrderStatusRules rule = OrderStatusRules.SHIPPED;

		switch (rule) {
		case SHIPPED: 
			if(OrderStatusRules.SHIPPED.getStatus().equals(status1) || OrderStatusRules.SHIPPED.getStatus().equals(status2))
				return OrderStatusRules.SHIPPED.getStatus();
		case RELEASED: 
			if(OrderStatusRules.RELEASED.getStatus().equals(status1) || OrderStatusRules.RELEASED.getStatus().equals(status2))
				return OrderStatusRules.RELEASED.getStatus();
		case DC_ALLOCATED: 
			if(OrderStatusRules.DC_ALLOCATED.getStatus().equals(status1) || OrderStatusRules.DC_ALLOCATED.getStatus().equals(status2))
				return OrderStatusRules.DC_ALLOCATED.getStatus();
		case DO_CREATED: 
			if(OrderStatusRules.DO_CREATED.getStatus().equals(status1) || OrderStatusRules.DO_CREATED.getStatus().equals(status2))
				return OrderStatusRules.DO_CREATED.getStatus();
		case ALLOCATED: 
			if(OrderStatusRules.ALLOCATED.getStatus().equals(status1) || OrderStatusRules.ALLOCATED.getStatus().equals(status2))
				return OrderStatusRules.ALLOCATED.getStatus();
		default: 
			return OrderStatusRules.BACK_ORDERED.getStatus();
		}
	}

	/**
	 * 
	 * @param orderLines
	 * @param firstOrderLine
	 */
	private Boolean applyOtherRules(List<OrderLine> orderLines, OrderLine firstOrderLine) {
		boolean isAllocated = orderLines.stream().filter(o -> !StringUtils.isEmpty(o.getOrderLineStatus()))
				.anyMatch(OrderStatusRules.ALLOCATED.getFilter());

		if (isAllocated)
			firstOrderLine.setOrderLineStatus(OrderStatusRules.ALLOCATED.getStatus());

		boolean isCancelled = orderLines.stream().filter(o -> !StringUtils.isEmpty(o.getOrderLineStatus()))
				.allMatch(OrderStatusRules.CANCELED.getFilter());
		if (isCancelled)
			firstOrderLine.setOrderLineStatus(OrderStatusRules.CANCELED.getStatus());
		
		if(!isCancelled) {
			isCancelled = orderLines.stream().filter(o -> !StringUtils.isEmpty(o.getOrderLineStatus()))
					.allMatch(OrderStatusRules.CANCELLED.getFilter());
			if(isCancelled)
				firstOrderLine.setOrderLineStatus(OrderStatusRules.CANCELLED.getStatus());
		}
		
		return Boolean.TRUE;
	}

	/**
	 * Defines each of the OrderLine status with the condition required for that
	 * rule
	 * 
	 * @author Ismail
	 *
	 */
	private enum OrderStatusRules {

		SHIPPED("Shipped", orderLine -> orderLine.getOrderLineStatus().equals("Shipped")),
		P_SHIPPED("Partially Shipped", orderLine -> orderLine.getOrderLineStatus().equals("Partially Shipped")),

		RELEASED("Released", orderLine -> orderLine.getOrderLineStatus().equals("Released")),
		P_RELEASED("Partially Released", orderLine -> orderLine.getOrderLineStatus().equals("Partially Released")),

		DC_ALLOCATED("DC Allocated", orderLine -> orderLine.getOrderLineStatus().equals("DC Allocated")),
		DO_CREATED("DO Created", orderLine -> orderLine.getOrderLineStatus().equals("DO Created")),
		BACK_ORDERED("Back Ordered", orderLine -> orderLine.getOrderLineStatus().equals("Back Ordered")),

		ALLOCATED("Allocated", orderLine -> orderLine.getOrderLineStatus().equals("Allocated")),
		CANCELLED("Cancelled", orderLine -> orderLine.getOrderLineStatus().equals("Cancelled")),
		
		CANCELED("Canceled", orderLine -> orderLine.getOrderLineStatus().equals("Canceled")); //Have seen Orders coming with status "Canceled" rather "Cancelled".

		private String status;
		private Predicate<OrderLine> filter;

		OrderStatusRules(String status, Predicate<OrderLine> filter) {
			this.status = status;
			this.filter = filter;
		}

		public Predicate<OrderLine> getFilter() {
			return this.filter;
		}

		public String getStatus() {
			return this.status;
		}
	}
}