package com.order.consolidator.services.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.order.consolidator.entities.TXML.Message.Order.OrderLines.OrderLine;
import com.order.consolidator.entities.TXML.Message.Order.OrderLines.OrderLine.TaxDetails.TaxDetail;
import com.order.consolidator.services.AbstractBaseConsolidation;
import com.order.consolidator.util.PredicateManager;

/**
 * Consolidates the linelevel amount for the Sales Tax, Shipping Tax, Handling
 * Tax and Misc Taxes undet the "TaxDetails".
 * 
 * @author Ismail
 *
 */

@Service
public class TaxTotalConsolidation extends AbstractBaseConsolidation<OrderLine> {

	private static final Logger LOGGER = LogManager.getLogger(TaxTotalConsolidation.class);

	@Override
	public void performConsolidation(List<OrderLine> orderLines) {
		if (!validateRequiredFields(orderLines))
			return;

		Optional<OrderLine> optional = orderLines.stream().findFirst();
		OrderLine firstOrderLine = optional.get();

		// Fetch distinct Tax Charge categories on the first orderline. For default
		// salestax its empty
		Set<String> chargeIds = firstOrderLine.getTaxDetails().getTaxDetail().stream()
				.filter(PredicateManager.getNotNullFilter()).map(c -> constructTaxCategoryId(c))
				.collect(Collectors.toSet());

		// Aggregate the Tax amount towards each type and apply to the first
		// orderline
		chargeIds.stream().forEach(chargeId -> {
			Predicate<TaxDetail> predicate = PredicateManager.getTaxChargeByCategory(chargeId);
			BigDecimal taxAmount = getTaxAmount(orderLines, predicate, chargeId);
			applyTaxAmount(firstOrderLine, taxAmount, predicate, chargeId);
			BigDecimal taxableAmount = getTaxableAmount(orderLines, predicate, chargeId);
			applyTaxableAmount(firstOrderLine, taxableAmount, predicate, chargeId);
		});

		updateTaxDetailIds(orderLines);
	}

	/**
	 * TaxDetails can be uniquely identified based on the combination of
	 * ChargeCategory, RequestType and ExternalTaxDetailId (if valid value is
	 * present).
	 * 
	 * @param taxDetail
	 * @return
	 */
	private String constructTaxCategoryId(TaxDetail taxDetail) {
		StringBuilder categoryId = new StringBuilder();
		categoryId.append(taxDetail.getTaxCategory()).append("~");
		categoryId.append(taxDetail.getChargeCategory().getValue()).append("~");
		categoryId.append(taxDetail.getRequestType());
		return categoryId.toString();
	}

	
	protected boolean validateRequiredFields(List<OrderLine> orderLines) {
		if (!super.validateRequiredFields(orderLines))
			return Boolean.FALSE;

		Optional<OrderLine> optional = orderLines.stream().findFirst();
		OrderLine firstOrderLine = optional.get();

		if (StringUtils.isEmpty(firstOrderLine.getTaxDetails())
				|| StringUtils.isEmpty(firstOrderLine.getTaxDetails().getTaxDetail())
				|| firstOrderLine.getTaxDetails().getTaxDetail().size() == 0)
			return Boolean.FALSE;

		return Boolean.TRUE;
	}

	/**
	 * For the given taxType and multi-quantity OrderLines, calculates the total tax amount.
	 * @param orderLines
	 * @param predicate
	 * @param chargeType
	 * @return
	 */
	private BigDecimal getTaxAmount(List<OrderLine> orderLines, Predicate<TaxDetail> predicate, Object chargeType) {
		return orderLines.stream().map(o -> o.getTaxDetails()).filter(Objects::nonNull)
				.flatMap(c -> c.getTaxDetail().stream()).filter(predicate).map(d -> d.getTaxAmount())
				.reduce(BigDecimal.ZERO, (b1, b2) -> b1.add(b2));
	}
	
	/**
	 * For the given taxType and multi-quantity OrderLines, calculates the total taxable amount.
	 * @param orderLines
	 * @param predicate
	 * @param chargeType
	 * @return
	 */
	private BigDecimal getTaxableAmount(List<OrderLine> orderLines, Predicate<TaxDetail> predicate, Object chargeType) {
		return orderLines.stream().map(o -> o.getTaxDetails()).filter(Objects::nonNull)
				.flatMap(c -> c.getTaxDetail().stream()).filter(predicate).map(d -> d.getTaxableAmount())
				.filter(PredicateManager.getNotNullFilter())
				.map(d -> d.getValue())
				.filter(PredicateManager.getNotEmptyFilter())
				.map(s ->new BigDecimal(s))
				.reduce(BigDecimal.ZERO, (b1, b2) -> b1.add(b2));
	}

	/**
	 * For the given OrderLine and TaxType updates the total taxAmount
	 * @param orderLine
	 * @param amount
	 * @param predicate
	 * @param chargeType
	 */
	private void applyTaxAmount(OrderLine orderLine, BigDecimal amount, Predicate<TaxDetail> predicate, Object chargeType) {
		LOGGER.info("applyTax Amount for orderLine:::{} amount:::{} taxType:::{}", orderLine.getItemID(), amount,
				chargeType);

		orderLine.getTaxDetails().getTaxDetail().stream().filter(predicate).forEach(d -> d.setTaxAmount(amount));
	}
	
	/**
	 * For the given OrderLine and TaxType updates the total Taxable amount
	 * @param orderLine
	 * @param amount
	 * @param predicate
	 * @param chargeType
	 */
	private void applyTaxableAmount(OrderLine orderLine, BigDecimal amount, Predicate<TaxDetail> predicate, Object chargeType) {
		LOGGER.info("applyTaxable Amount for orderLine:::{} amount:::{} taxType:::{}", orderLine.getItemID(), amount,
				chargeType);

		orderLine.getTaxDetails().getTaxDetail().stream().filter(predicate).forEach(d -> d.getTaxableAmount().setValue(amount.toPlainString()));
	}

	/**
	 * Updates the OrderLine TaxDetails with the original LineNumbers
	 * 
	 * @param orderLines
	 */
	private void updateTaxDetailIds(List<OrderLine> orderLines) {
		Optional<OrderLine> optional = orderLines.stream().findFirst();
		OrderLine firstOrderLine = optional.get();

		String refField15 = firstOrderLine.getLineReferenceFields().getReferenceField15().getValue();
		firstOrderLine.getTaxDetails().getTaxDetail().stream().forEach(t -> updateTaxDetails(t, refField15));
	}

	/**
	 * Updates the LineNbr with the provided original for the Tax Details
	 * 
	 * @param taxDetail
	 * @param refField15
	 */
	private void updateTaxDetails(TaxDetail taxDetail, String refField15) {
		if (taxDetail != null && taxDetail.getExtTaxDetailId() != null) {
			String extTaxDetailId = taxDetail.getExtTaxDetailId();
			int index = extTaxDetailId.indexOf("-");
			if (index != -1) {
				String prefix = extTaxDetailId.substring(0, index + 1);
				String newId = extTaxDetailId.replace(prefix, String.valueOf(refField15) + "-");
				taxDetail.setExtTaxDetailId(newId);
			} else {
				//TODO: Pending requirement on what needs to be done for the externalTaxDetailId with the line#
				int size = extTaxDetailId.length();
			//	String newId = extTaxDetailId.repl
			//	taxDetail.setExtTaxDetailId(extTaxDetailId.);
			}
		}
	}
}
