package com.order.consolidator.services.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import com.order.consolidator.entities.TXML.Message.Order.DistributionOrders.DistributionOrder;
import com.order.consolidator.entities.TXML.Message.Order.DistributionOrders.DistributionOrder.LineItems.LineItem;
import com.order.consolidator.services.AbstractBaseConsolidation;


/**
 * 
 * @author Ismail
 *
 */
@Service
public class DistributionOrderConsolidation extends AbstractBaseConsolidation<DistributionOrder> {

	@Override
	public void performConsolidation(List<DistributionOrder> distributionOrders) {
		if (!validateRequiredFields(distributionOrders))
			return;
		
		consolidateDistributionOrders(distributionOrders);
	}

	protected boolean validateRequiredFields(List<DistributionOrder> distributionOrders) {
		if (!super.validateRequiredFields(distributionOrders))
			return Boolean.FALSE;

		Optional<DistributionOrder> optional = distributionOrders.stream().findFirst();
		DistributionOrder firstDistributionOrder = optional.get();

		if (StringUtils.isEmpty(firstDistributionOrder.getDONbr()) || firstDistributionOrder.getLineItems() == null
				|| firstDistributionOrder.getLineItems().getLineItem() == null
				|| firstDistributionOrder.getLineItems().getLineItem().size() == 0)
			return Boolean.FALSE;

		return Boolean.TRUE;
	}

	private void consolidateDistributionOrders(List<DistributionOrder> distributionOrders) {
		distributionOrders.stream()
				.forEach(distributionOrder -> consolidateDOLineItems(distributionOrder, distributionOrder.getLineItems().getLineItem()));
	}

	private void consolidateDOLineItems(DistributionOrder distributionOrder, List<LineItem> lineItems) {
		//Create a mapping of DOLineNumber and the LineItem with that DOLineNbr
		Map<String, List<LineItem>> mapping = lineItems.stream()
		.collect(Collectors.groupingBy(lineItem -> lineItem.getDOLineNbr().toString()));
		
		List<LineItem> consolidatedLineItems = 
		mapping.entrySet().stream()
		.map(entry -> consolidateLineItemQuantity(entry.getValue()))
		.collect(Collectors.toList());
		distributionOrder.getLineItems().getLineItem().clear();
		distributionOrder.getLineItems().getLineItem().addAll(consolidatedLineItems);
	}

	private LineItem consolidateLineItemQuantity(List<LineItem> lineItems) {
		BigDecimal orderedQuantity = 
				lineItems.stream().map(lineItem -> lineItem.getOrderedQty())
		.reduce(BigDecimal.ZERO, (a,b) -> a.add(b));
		
		BigDecimal shippedQuantity = 
				lineItems.stream()
				.filter(lineItem -> !ObjectUtils.isEmpty(lineItem.getShippedQuantity())
						&& !ObjectUtils.isEmpty(lineItem.getShippedQuantity().getValue()))
				.map(lineItem -> lineItem.getShippedQuantity().getValue())
				.map(val -> new BigDecimal(val))
		.reduce(BigDecimal.ZERO, (a,b) -> a.add(b));
		
		BigDecimal POLIOrderdedQty = 
				lineItems.stream().map(lineItem -> lineItem.getPOLIOrderdedQty())
		.reduce(BigDecimal.ZERO, (a,b) -> a.add(b));
		
		LineItem firstItem = lineItems.get(0);
		firstItem.setOrderedQty(orderedQuantity);
		firstItem.setPOLIOrderdedQty(POLIOrderdedQty);
		firstItem.getShippedQuantity().setValue(shippedQuantity.toPlainString());
		
		return firstItem;
	}

}
