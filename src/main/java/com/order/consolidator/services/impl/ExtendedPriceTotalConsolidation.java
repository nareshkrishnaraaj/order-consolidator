package com.order.consolidator.services.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.order.consolidator.entities.TXML.Message.Order.OrderLines.OrderLine;
import com.order.consolidator.services.AbstractBaseConsolidation;

/**
 * Consolidates the PriceInfo::ExtendedPrice:Price
 * 
 * @author Ismail
 *
 */

@Service
public class ExtendedPriceTotalConsolidation extends AbstractBaseConsolidation<OrderLine> {

	private static final Logger LOGGER = LogManager.getLogger(ExtendedPriceTotalConsolidation.class);

	@Override
	public void performConsolidation(List<OrderLine> orderLines) {
		if (!validateRequiredFields(orderLines))
			return;

		Optional<OrderLine> optional = orderLines.stream().findFirst();
		OrderLine firstOrderLine = optional.get();

		BigDecimal extendedPrice = orderLines.stream().map(o -> o.getPriceInfo()).filter(Objects::nonNull)
				.map(o -> o.getExtendedPrice()).filter(Objects::nonNull).filter(o -> !StringUtils.isEmpty(o.getValue()))
				.map(o -> new BigDecimal(o.getValue()).setScale(2, RoundingMode.FLOOR))
				.reduce(BigDecimal.ZERO, (a, b) -> a.add(b));

		firstOrderLine.getPriceInfo().getExtendedPrice()
				.setValue(extendedPrice.setScale(2, RoundingMode.FLOOR).toPlainString());

		LOGGER.info("ExtendPrice for {} updated to {}", firstOrderLine.getItemID(), extendedPrice);
	}

	protected boolean validateRequiredFields(List<OrderLine> orderLines) {
		if (!super.validateRequiredFields(orderLines))
			return Boolean.FALSE;

		Optional<OrderLine> optional = orderLines.stream().findFirst();
		OrderLine firstOrderLine = optional.get();

		if (StringUtils.isEmpty(firstOrderLine.getPriceInfo())
				|| StringUtils.isEmpty(firstOrderLine.getPriceInfo().getExtendedPrice())
				|| StringUtils.isEmpty(optional.get().getPriceInfo().getExtendedPrice().getValue()))
			return Boolean.FALSE;

		return Boolean.TRUE;
	}
}
