package com.order.consolidator.services.impl;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.annotation.After;
import org.springframework.stereotype.Service;

import com.order.consolidator.entities.TXML;
import com.order.consolidator.entities.TXML.Message.Order.DistributionOrders.DistributionOrder;
import com.order.consolidator.entities.TXML.Message.Order.DistributionOrders.DistributionOrder.LineItems;
import com.order.consolidator.entities.TXML.Message.Order.OrderLines.OrderLine;
import com.order.consolidator.entities.TXML.Message.Order.ShipmentDetails.ShipmentDetail.Cartons.Carton;

/**
 * This class handles the orderline status fix for the issue AE-6316, which is a defect from DOM.
 * Issue1: If there are multi-quantity orderLine, despite of all the quantity being shipped, OrderLine
 * status is received as "Back Ordered" for 1 or more of the quantities. Impact: Incorrect
 * shipping status in the Shipment email.
 * Issue2: If there's a single quantity order, despite of shipping the orderline, orderline
 * status comes as back-ordered from DOM. Impact - No Shipment email is triggered, since orderline
 * status is set to blank after status consolidation.
 * @author Ismail
 *
 */

@Service
public class OrderLineStatusPatch {

	private static final Logger LOGGER = LogManager.getLogger(OrderLineStatusPatch.class);
	
	@After("execution(* com.order.consolidator.services.impl.OrderLineStatusUpdateImpl.consolidate(..))")
	public List<OrderLine> consolidate(List<OrderLine> orderLines, TXML txml) {
		LOGGER.info("Applying OrderLine Status Patch Rules...");
		Optional<OrderLine> firstLine = orderLines.stream().findFirst();
		if (firstLine.isPresent()) {
			LOGGER.info("Triggering rules for {}", firstLine.get().getLineNumber());
			//applyRules(orderLines, firstLine.get(),txml);
		}
		LOGGER.info("Updated the OrderLines status Patch based on the rules.");
		return orderLines;
	}
	
	/**
	 * Rules:
	 * If there's one or more orderlines' status back-ordered and they have a corresponding shipmentNbr, update the status as Shipped.
	 * @param orderLines
	 * @param firstOrderLine
	 */
	protected void applyRules(List<OrderLine> orderLines, OrderLine firstOrderLine,TXML txml) {
		List<OrderLine> backOrderedLines = 
				orderLines.stream().filter(orderLine -> orderLine.getOrderLineStatus().equalsIgnoreCase("Back Ordered")).collect(Collectors.toList());
		//If there are items having "back ordered" status, lookup their DistributionOrders using the OrderLine# and COLineNbr.
		//The OrderLine# and COLine# will match before consolidating the quantities (HERE).
		//Using the DOLineNbr and DONbr, lookup their ShipmentDetails.
		//If all those items have shipment, the orderline status should to updated as Shipped
		
		if(backOrderedLines.size() > 0) {
			
			List<DistributionOrder> distributionOrders = txml.getMessage().getOrder().get(0).getDistributionOrders().getDistributionOrder();
			
			//Create a Map of COLineNbr : DONbr ~ DOLineNbr 
			Map<String,LineItems> map =		
			distributionOrders.stream().collect(Collectors.toMap(DistributionOrder::getDONbr, DistributionOrder::getLineItems));
			
			List<Carton> cartons = txml.getMessage().getOrder().get(0).getShipmentDetails().getShipmentDetail().getCartons().getCarton();
			if(!Objects.isNull(cartons)) {
				
			}
		}
	}
}
