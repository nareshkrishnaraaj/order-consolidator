package com.order.consolidator.services.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.order.consolidator.entities.TXML;
import com.order.consolidator.entities.TXML.Message.Order.DistributionOrders.DistributionOrder;
import com.order.consolidator.entities.TXML.Message.Order.DistributionOrders.DistributionOrder.LineItems;
import com.order.consolidator.entities.TXML.Message.Order.DistributionOrders.DistributionOrder.LineItems.LineItem;
import com.order.consolidator.entities.TXML.Message.Order.Invoices.InvoiceDetail;
import com.order.consolidator.entities.TXML.Message.Order.OrderLines.OrderLine;
import com.order.consolidator.entities.TXML.Message.Order.ShipmentDetails.ShipmentDetail.Cartons.Carton;
import com.order.consolidator.entities.TXML.Message.Order.ShipmentDetails.ShipmentDetail.Cartons.Carton.CartonDetails.CartonDetail;

/**
 * Restructure the orderlines based on the parent orderLine# and drops the duplicate orderlines. 
 * Map DistributionOrderLines with the original OrderLines 
 * Map the InvoiceDetails with the Original OrderLines
 * 
 * @author Ismail
 *
 */
@Service
public class AlignOrderLines {

	private static final Logger LOGGER = LogManager.getLogger(AlignOrderLines.class);

	public void alignOrderLines(Map<String, String> lineNbrRefField15Map, TXML orderXml) {

		LOGGER.info("Aligning OrderLine Ids");
		mapOriginalOrderLineIds(orderXml, lineNbrRefField15Map);
		mapExtTaxDetailsIds(orderXml);
		mapDistributionOrderIds(orderXml, lineNbrRefField15Map);
		mapInvoiceDetails(orderXml, lineNbrRefField15Map);
	}

	/**
	 * OrderLine RefField15 is mapped towards the OrderLine:LineNumber and
	 * LineReferenceField1
	 * 
	 * @param orderXml
	 * @param refField15LinenumMap
	 */
	private void mapOriginalOrderLineIds(TXML orderXml, Map<String, String> lineNbrRefField15Map) {
		LOGGER.info("Aligning OrderLine OrderLine:LineNumbers");
		List<OrderLine> orderLines = orderXml.getMessage().getOrder().get(0).getOrderLines().getOrderLine();
		orderLines.stream().forEach(orderLine -> orderLine.getLineReferenceFields().getReferenceField1().setValue(String
				.format("%08d", Integer.valueOf(lineNbrRefField15Map.get(orderLine.getLineNumber()))).concat("00000")));
		orderLines.stream()
		.forEach(orderLine -> orderLine.setLineNumber(lineNbrRefField15Map.get(orderLine.getLineNumber())));
	}

	/**
	 * Updates the LineLevel ExtTaxDetailId to reflect the original line#as the last
	 * 1 digit and 2 digits if the line# is of 2 digits. The requirements are very
	 * ambiguous for this field and are suspected as not used by any downstream
	 * system. If found to be used, may need a correction accordingly.
	 * 
	 * @param orderXml
	 * @param lineNbrRefField15Map
	 */
	private void mapExtTaxDetailsIds(TXML orderXml) {
		LOGGER.info("Updating the ExtTaxDetailsIds based on the consolidatedOrderLineIds.");
		List<OrderLine> orderLines = orderXml.getMessage().getOrder().get(0).getOrderLines().getOrderLine();
		orderLines.stream().forEach(orderLine -> constructExtTaxDetailId(orderLine));
	}

	/**
	 * Constructs the Ext Tax DetailId in the format : line#as the last 1 digit and
	 * 2 digits if the line# is of 2 digits
	 * 
	 * @param orderLine
	 */
	private void constructExtTaxDetailId(OrderLine orderLine) {
		orderLine.getTaxDetails().getTaxDetail().stream().filter(t -> t.getRequestType().equals("ADJUSTMENT"))
		.forEach(taxDetail -> {
			String extTaxDetailId = taxDetail.getExtTaxDetailId();
			if (!StringUtils.isEmpty(extTaxDetailId) && extTaxDetailId.length() > 2) {
				String newTaxDetailId = extTaxDetailId.substring(0, extTaxDetailId.length() - 2)
						+ orderLine.getLineNumber();
				taxDetail.setExtTaxDetailId(newTaxDetailId);
			}
		});
	}

	/**
	 * OrderLine RefField15 is mapped towards the DistributionOrder:LineItem
	 * ->COLineNbr
	 * 
	 * @param orderLines
	 */
	private void mapDistributionOrderIds(TXML orderXml, Map<String, String> lineNbrRefField15Map) {

		LOGGER.info("Aligning Distribution Ids");
		List<DistributionOrder> distributionOrders = orderXml.getMessage().getOrder().get(0).getDistributionOrders()
				.getDistributionOrder();

		//		distributionOrders.stream().flatMap(d -> d.getLineItems().getLineItem().stream())
		//				.forEach(l -> l.setCOLineNbr(lineNbrRefField15Map.get(l.getCOLineNbr())));

		// Changing to consolidate the DistributionLineItems and
		// ShipmentDetails>Carton>CartonDetail

		Map<String, String> coDoLineNbrMapping = new HashMap<String, String>();
		
		//Fetch the DoNbr to List of LineItems Mapping
		Map<String, List<LineItem>> doNbrLineItemMap = new HashMap<String, List<LineItem>>();
		distributionOrders.stream()
		.forEach(distribution ->  {
			doNbrLineItemMap.put(distribution.getDONbr(), distribution.getLineItems().getLineItem());
		});
		
		//Create a Map of DoNbr~DoLineNbr to the Original ParentLineNbr and assign the same ParentLineNbr to the LineItem in Distribution
		doNbrLineItemMap.entrySet().stream() //iterating over each pair of DoNbr and the list of LineItems.
		.forEach(entry -> {
			String doNbr = entry.getKey();
			List<LineItem> lineItems = entry.getValue();
			lineItems.stream()	//Now iterating over Each of the LineItem for the specific DONbr
			.forEach(lineItem -> {
				
				String coLineNo = lineItem.getCOLineNbr();	//Get the current COLine# for the Item

				// create a mapping of current DOLineNbr and the new COLineNbr
				// that will be assigned to update the ShipmentDetails with the same DOLineNbr. 
				//This needs to be created before the DOLineNbr is updated in the next step.
				coDoLineNbrMapping.put(doNbr +"~"+lineItem.getDOLineNbr(), lineNbrRefField15Map.get(coLineNo));

				// Assign the Actual parentOrderLineNbr
				lineItem.setCOLineNbr(lineNbrRefField15Map.get(coLineNo));
				lineItem.setDOLineNbr(lineNbrRefField15Map.get(coLineNo));

			});
		});
		
		//Update the ShipmentDetails with the DOLineNbr based on the mapping above.
		mapShipmentDetails(orderXml,coDoLineNbrMapping);
	}
	
	private void mapShipmentDetails(TXML orderXml, Map<String, String> coDoLineNbrMapping) {
		// If there are Shipmentdetail and CartonDetails present, update the DOLineNbr
		// of the items in the Carton to reflect the original LineNbr from Reffield15.
		if (orderXml.getMessage().getOrder().get(0).getShipmentDetails() != null
				&& orderXml.getMessage().getOrder().get(0).getShipmentDetails().getShipmentDetail() != null
				&& orderXml.getMessage().getOrder().get(0).getShipmentDetails().getShipmentDetail().getCartons() != null
				&& orderXml.getMessage().getOrder().get(0).getShipmentDetails().getShipmentDetail().getCartons()
				.getCarton() != null
				&& orderXml.getMessage().getOrder().get(0).getShipmentDetails().getShipmentDetail().getCartons()
				.getCarton().size() > 0) {
			LOGGER.info("Aligning Shipment DOLineNbrs");
			List<Carton> cartons = orderXml.getMessage().getOrder().get(0).getShipmentDetails().getShipmentDetail()
					.getCartons().getCarton();
			
			
			for(Carton carton: cartons) {
				String doNbr = carton.getDONbr();
				String shipmentNbr = carton.getShipmentNbr();
				LOGGER.info("doNbr::{} and shipmentNbr::{}",doNbr,shipmentNbr);
				List<CartonDetail> cartonDetails = carton.getCartonDetails().getCartonDetail();
				cartonDetails.stream().forEach(cartonDetail -> {
					cartonDetail.setDOLineNbr(coDoLineNbrMapping.get(doNbr +"~"+cartonDetail.getDOLineNbr()));
				});
			}
		}
	}
	

	/**
	 * OrderLine RefField15 is mapped toward the
	 * InvoiceDetails:Invoices:InvoiceLineDetail->ParentOrderLineNbr
	 * 
	 * @param orderXml
	 * @param refField15LinenumMap
	 */
	private void mapInvoiceDetails(TXML orderXml, Map<String, String> lineNbrRefField15Map) {

		LOGGER.info("Aligning InvoiceDetails Ids");
		List<InvoiceDetail> invoiceDetails = orderXml.getMessage().getOrder().get(0).getInvoices().getInvoiceDetail();

		invoiceDetails.stream().flatMap(i -> i.getInvoiceLines().getInvoiceLineDetail().stream())
		.forEach(i -> i.setParentOrderLineNbr(lineNbrRefField15Map.get(i.getParentOrderLineNbr())));
	}
}
