package com.order.consolidator.services.impl;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import com.order.consolidator.entities.TXML.Message.Order.ShipmentDetails.ShipmentDetail.Cartons.Carton.CartonDetails.CartonDetail;
import com.order.consolidator.services.AbstractBaseConsolidation;

/**
 * Consolidates the quantity of the items in each carton. All the items in the
 * Carton have the same DONbr and the DOLineNbr in CartonDetail matches with
 * that of the DOLineNbr in DistributionOrders having the same DONbr as the
 * Carton.
 * 
 * @author Ismail
 *
 */
@Service
public class ShipmentDetailConsolidation extends AbstractBaseConsolidation<CartonDetail> {

	@Override
	public void performConsolidation(List<CartonDetail> cartonDetails) {
		if(!validateRequiredFields(cartonDetails))
			return;
		
		Map<String,List<CartonDetail>> groupedCartonDetails =
		cartonDetails.stream().collect(Collectors.groupingBy(cartonDetail -> cartonDetail.getDOLineNbr()));
		
		List<CartonDetail> consolidatedCartonDetails = 
				groupedCartonDetails.entrySet().stream().map(entry -> consolidateQuantityByItem(entry.getValue())).collect(Collectors.toList());
		
		cartonDetails.clear();
		cartonDetails.addAll(consolidatedCartonDetails);
		
		//TODO:
		//firstCartonDetail.setDOLineNbr(value); //update DOLineNbr same as what is update at DistributionLevel.
	}

	protected boolean validateRequiredFields(List<CartonDetail> cartonDetails) {
		if (!super.validateRequiredFields(cartonDetails))
			return Boolean.FALSE;

		Optional<CartonDetail> optional = cartonDetails.stream().findFirst();
		CartonDetail firstCartonDetail = optional.get();

		if (StringUtils.isEmpty(firstCartonDetail.getQuantity())
				|| StringUtils.isEmpty(firstCartonDetail.getDOLineNbr()))
			return Boolean.FALSE;

		return Boolean.TRUE;
	}

	private CartonDetail consolidateQuantityByItem(List<CartonDetail> cartonDetails) {
		if (ObjectUtils.isEmpty(cartonDetails))
			return null;

		BigDecimal totalQty = cartonDetails.stream().map(carton -> carton.getQuantity()).reduce(BigDecimal.ZERO,
				(a, b) -> a.add(b));
		Optional<CartonDetail> optional = cartonDetails.stream().findFirst();
		CartonDetail carton = optional.isPresent() ? optional.get() : null;

		if (optional.isPresent())
			carton.setQuantity(totalQty);
		return carton;
	}

}
