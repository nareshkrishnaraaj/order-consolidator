package com.order.consolidator.services.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.order.consolidator.entities.TXML.Message.Order.Invoices.InvoiceDetail.InvoiceLines.InvoiceLineDetail;
import com.order.consolidator.services.AbstractBaseConsolidation;
import com.order.consolidator.util.PredicateManager;

/**
 * Consolidates the base attributes of InvoiceLineDetails
 * 
 * @author Ismail
 *
 */
@Service
public class InvoiceLineBaseConsolidation extends AbstractBaseConsolidation<InvoiceLineDetail> {

	@Override
	public void performConsolidation(List<InvoiceLineDetail> invoiceLineDetails) {
		if (!validateRequiredFields(invoiceLineDetails))
			return;
		consolidateBaseDetails(invoiceLineDetails);
	}

	protected boolean validateRequiredFields(List<InvoiceLineDetail> invoiceLineDetails) {
		if (!super.validateRequiredFields(invoiceLineDetails))
			return Boolean.FALSE;
		return Boolean.TRUE;
	}

	/**
	 * Consolidates all the base details of InvoiceLineDetails. InvoiceLineNbr will
	 * be updated to reflect the first invoiceLineNbr, since we found no
	 * significance of this fields for the consuming downstream systems.
	 * ParentOrderLineNbr is expected to be same for all the LineNbrs that need to
	 * be grouped. Rest all other details will be aggregated.
	 * 
	 * @param invoiceLineDetails
	 */
	private void consolidateBaseDetails(List<InvoiceLineDetail> invoiceLineDetails) {

		// consolidate OrderedQty,ShippedQuantity,InvoicedQuantity,LineCharges,LineTax,LineDiscounts,LineTotal,LineSubTotal

		Optional<InvoiceLineDetail> optional = invoiceLineDetails.stream().findFirst();
		InvoiceLineDetail firstOrderLine = optional.get();

		// OrderedQuanitty
		BigDecimal orderedQuantity = invoiceLineDetails.stream().map(invoiceLine -> invoiceLine.getOrderedQty())
				.reduce(BigDecimal.ZERO, (a, b) -> a.add(b));
		firstOrderLine.setOrderedQty(orderedQuantity);

		// InvoicedQuantity
		BigDecimal invoicedQuantity = invoiceLineDetails.stream().map(invoiceLine -> invoiceLine.getInvoicedQuantity())
				.reduce(BigDecimal.ZERO, (a, b) -> a.add(b));
		firstOrderLine.setInvoicedQuantity(invoicedQuantity);

		// ShippedQuantity
		BigDecimal shippedQuantity = invoiceLineDetails.stream().map(invoiceLine -> invoiceLine.getShippedQuantity())
				.filter(PredicateManager.getNotNullFilter()).map(jaxbObj -> jaxbObj.getValue())
				.filter(PredicateManager.getNotNullFilter()).map(value -> new BigDecimal(value))
				.reduce(BigDecimal.ZERO, (a, b) -> a.add(b));
		if (shippedQuantity.compareTo(BigDecimal.ZERO) > 0)
			firstOrderLine.getShippedQuantity().setValue(shippedQuantity.toPlainString());

		// LineCharges
		BigDecimal lineCharges = invoiceLineDetails.stream().map(invoiceLine -> invoiceLine.getLineCharges())
				.filter(PredicateManager.getNotNullFilter()).map(jaxbObj -> jaxbObj.getValue())
				.filter(PredicateManager.getNotNullFilter()).map(value -> new BigDecimal(value))
				.reduce(BigDecimal.ZERO, (a, b) -> a.add(b));
		if (lineCharges.compareTo(BigDecimal.ZERO) > 0)
			firstOrderLine.getLineCharges().setValue(lineCharges.toPlainString());

		// LineTax
		BigDecimal lineTaxes = invoiceLineDetails.stream().map(invoiceLine -> invoiceLine.getLineTax())
				.filter(PredicateManager.getNotNullFilter()).map(jaxbObj -> jaxbObj.getValue())
				.filter(PredicateManager.getNotNullFilter()).map(value -> new BigDecimal(value))
				.reduce(BigDecimal.ZERO, (a, b) -> a.add(b));
		if (lineTaxes.compareTo(BigDecimal.ZERO) > 0)
			firstOrderLine.getLineTax().setValue(lineTaxes.toPlainString());

		// LineDiscounts
		BigDecimal lineDiscounts = invoiceLineDetails.stream().map(invoiceLine -> invoiceLine.getLineDiscounts())
				.filter(PredicateManager.getNotNullFilter()).map(jaxbObj -> jaxbObj.getValue())
				.filter(PredicateManager.getNotNullFilter()).map(value -> new BigDecimal(value))
				.reduce(BigDecimal.ZERO, (a, b) -> a.add(b));
		if (lineDiscounts.compareTo(BigDecimal.ZERO) > 0)
			firstOrderLine.getLineDiscounts().setValue(lineDiscounts.toPlainString());

		// LineSubTotal
		BigDecimal lineSubTotal = invoiceLineDetails.stream().map(invoiceLine -> invoiceLine.getLineSubTotal())
				.filter(PredicateManager.getNotNullFilter()).map(jaxbObj -> jaxbObj.getValue())
				.filter(PredicateManager.getNotNullFilter()).map(value -> new BigDecimal(value))
				.reduce(BigDecimal.ZERO, (a, b) -> a.add(b));
		if (lineSubTotal.compareTo(BigDecimal.ZERO) > 0)
			firstOrderLine.getLineSubTotal().setValue(lineSubTotal.toPlainString());

		// LineTotal
		BigDecimal lineTotal = invoiceLineDetails.stream().map(invoiceLine -> invoiceLine.getLineTotal())
				.filter(PredicateManager.getNotNullFilter()).map(jaxbObj -> jaxbObj.getValue())
				.filter(PredicateManager.getNotNullFilter()).map(value -> new BigDecimal(value))
				.reduce(BigDecimal.ZERO, (a, b) -> a.add(b));
		if (lineTotal.compareTo(BigDecimal.ZERO) > 0)
			firstOrderLine.getLineTotal().setValue(lineTotal.toPlainString());
	}
}
