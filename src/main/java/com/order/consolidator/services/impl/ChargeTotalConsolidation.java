package com.order.consolidator.services.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.order.consolidator.entities.TXML.Message.Order.OrderLines.OrderLine;
import com.order.consolidator.entities.TXML.Message.Order.OrderLines.OrderLine.ChargeDetails.ChargeDetail;
import com.order.consolidator.services.AbstractBaseConsolidation;
import com.order.consolidator.util.PredicateManager;

/**
 * Consolidates the Linelevel amount for Shipping, Handling and Misc under
 * "ChargeDetails"
 * 
 * @author Ismail
 */

@Service
public class ChargeTotalConsolidation extends AbstractBaseConsolidation<OrderLine> {

	private static final Logger LOGGER = LogManager.getLogger(ChargeTotalConsolidation.class);

	@Override
	public void performConsolidation(List<OrderLine> orderLines) {
		if (!validateRequiredFields(orderLines))
			return;

		Optional<OrderLine> optional = orderLines.stream().findFirst();
		OrderLine firstOrderLine = optional.get();

		// Fetch distinct charge categories on the first orderline
		Set<String> chargeIds = firstOrderLine.getChargeDetails().getChargeDetail().stream()
				.filter(PredicateManager.getNotNullFilter()).map(c -> c.getChargeCategory())
				.collect(Collectors.toSet());

		// Aggregate the charge amount towards each type and apply to the first
		// orderline
		chargeIds.stream().forEach(chargeId -> {
			Predicate<ChargeDetail> predicate = PredicateManager.getChargeByCategory(chargeId);
			BigDecimal chargeAmount = getChargeAmount(orderLines, predicate, chargeId);
			LOGGER.debug("{} Charge amount:::{}", chargeId, chargeAmount);
			applyCharge(firstOrderLine, chargeAmount, predicate, chargeId);
		});

		updateChargeDetails(orderLines);
	}

	protected boolean validateRequiredFields(List<OrderLine> orderLines) {
		if (!super.validateRequiredFields(orderLines))
			return Boolean.FALSE;

		Optional<OrderLine> optional = orderLines.stream().findFirst();
		OrderLine firstOrderLine = optional.get();

		if (StringUtils.isEmpty(firstOrderLine.getChargeDetails())
				|| StringUtils.isEmpty(firstOrderLine.getChargeDetails().getChargeDetail())
				|| firstOrderLine.getChargeDetails().getChargeDetail().size() == 0)
			return Boolean.FALSE;
		return Boolean.TRUE;
	}

	private BigDecimal getChargeAmount(List<OrderLine> orderLines, Predicate<ChargeDetail> predicate,
			Object chargeType) {
		return orderLines.stream().map(o -> o.getChargeDetails()).filter(Objects::nonNull)
				.flatMap(c -> c.getChargeDetail().stream()).filter(predicate).map(d -> d.getChargeAmount())
				.reduce(BigDecimal.ZERO, (b1, b2) -> b1.add(b2));
	}

	private void applyCharge(OrderLine orderLine, BigDecimal amount, Predicate<ChargeDetail> predicate,
			Object chargeType) {
		LOGGER.info("applyCharge for orderLine:::{} amount:::{} chargeType:::{}", orderLine.getItemID(), amount,
				chargeType);
		orderLine.getChargeDetails().getChargeDetail().stream().filter(predicate).forEach(d -> {
			d.setChargeAmount(amount);
			d.getOriginalChargeAmount().setValue(amount.toPlainString());
		});
	}

	private void updateChargeDetails(List<OrderLine> orderLines) {
		Optional<OrderLine> optional = orderLines.stream().findFirst();
		OrderLine firstOrderLine = optional.get();

		String refField15 = firstOrderLine.getLineReferenceFields().getReferenceField15().getValue();
		firstOrderLine.getChargeDetails().getChargeDetail().stream().forEach(t -> updateChargeDetailIds(t, refField15));
	}

	private void updateChargeDetailIds(ChargeDetail taxDetail, String refField15) {
		if (taxDetail != null && taxDetail.getExtChargeDetailId() != null) {
			int index = taxDetail.getExtChargeDetailId().indexOf("-");
			if (index != -1) {
				String prefix = taxDetail.getExtChargeDetailId().substring(0, index + 1);
				String newId = taxDetail.getExtChargeDetailId().replace(prefix, String.valueOf(refField15) + "-");
				taxDetail.setExtChargeDetailId(newId);
			}
		}
	}

}
