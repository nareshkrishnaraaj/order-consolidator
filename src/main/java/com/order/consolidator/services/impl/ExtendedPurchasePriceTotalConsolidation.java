package com.order.consolidator.services.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.order.consolidator.entities.TXML.Message.Order.OrderLines.OrderLine;
import com.order.consolidator.services.AbstractBaseConsolidation;

/**
 * Consolidates the Extended Purchase Price Custom Field.
 * 
 * @author Ismail
 *
 */

@Service
public class ExtendedPurchasePriceTotalConsolidation extends AbstractBaseConsolidation<OrderLine> {

	private static final Logger LOGGER = LogManager.getLogger(ExtendedPurchasePriceTotalConsolidation.class);

	@Override
	public void performConsolidation(List<OrderLine> orderLines) {
		if (!validateRequiredFields(orderLines))
			return;

		Optional<OrderLine> optional = orderLines.stream().findFirst();
		OrderLine firstOrderLine = optional.get();

		BigDecimal extendedPriceTotal = orderLines.stream()
				.flatMap(o -> o.getCustomFieldList().getCustomField().stream())
				.filter(Objects::nonNull)
				.filter(customField -> !Objects.isNull(customField.getName()) && !Objects.isNull(customField.getName().getValue()))
				.filter(customField -> !Objects.isNull(customField.getValue()) && !StringUtils.isEmpty(customField.getValue().getValue()))
				.filter(customField -> customField.getName().getValue().equals("ExtendedPurchasePrice"))
				.map(epp -> new BigDecimal(epp.getValue().getValue()))
				.reduce(BigDecimal.ZERO, (epp1, epp2) -> epp1.add(epp2));

		firstOrderLine.getCustomFieldList().getCustomField().stream().filter(Objects::nonNull)
				.filter(f -> f.getName().getValue().equals("ExtendedPurchasePrice")).forEach(
						f -> f.getValue().setValue(extendedPriceTotal.setScale(2, RoundingMode.FLOOR).toPlainString()));

		LOGGER.info("ExtendPurchasePrice for {} updated to {}", firstOrderLine.getItemID(), extendedPriceTotal);
	}

	protected boolean validateRequiredFields(List<OrderLine> orderLines) {

		if (!super.validateRequiredFields(orderLines))
			return Boolean.FALSE;

		Optional<OrderLine> optional = orderLines.stream().findFirst();
		OrderLine firstOrderLine = optional.get();

		if (StringUtils.isEmpty(firstOrderLine.getCustomFieldList())
				|| StringUtils.isEmpty(firstOrderLine.getCustomFieldList().getCustomField()))
			return Boolean.FALSE;

		return Boolean.TRUE;
	}
}
