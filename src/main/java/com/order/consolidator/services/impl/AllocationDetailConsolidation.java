package com.order.consolidator.services.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.order.consolidator.entities.TXML.Message.Order.OrderLines.OrderLine;
import com.order.consolidator.entities.TXML.Message.Order.OrderLines.OrderLine.AllocationDetails.AllocationDetail;
import com.order.consolidator.services.AbstractBaseConsolidation;
import com.order.consolidator.util.PredicateManager;

/**
 * Consolidates the AllocationDetails at the OrderLine level, the
 * AllocatedQuantity and eliminates the
 * 
 * @author Ismail
 *
 */

@Service
public class AllocationDetailConsolidation extends AbstractBaseConsolidation<OrderLine> {

	private static final Logger LOGGER = LogManager.getLogger(AllocationDetailConsolidation.class);

	@Override
	public void performConsolidation(List<OrderLine> orderLines) {
		if (!validateRequiredFields(orderLines))
			return;

		Optional<OrderLine> optional = orderLines.stream().findFirst();
		consolidateAllocationDetails(orderLines, optional.get());
	}

	protected boolean validateRequiredFields(List<OrderLine> orderLines) {

		if (!super.validateRequiredFields(orderLines))
			return Boolean.FALSE;

		Optional<OrderLine> optional = orderLines.stream().findFirst();
		OrderLine firstOrderLine = optional.get();

		if (ObjectUtils.isEmpty(firstOrderLine.getAllocationDetails())
				|| ObjectUtils.isEmpty(firstOrderLine.getAllocationDetails().getAllocationDetail())
				|| firstOrderLine.getAllocationDetails().getAllocationDetail().size() == 0)
			return Boolean.FALSE;

		return Boolean.TRUE;
	}

	/**
	 * Consolidates the AllocationDetails at each orderLines and assign against the
	 * first orderLine. If the allocationDetails are same then there should be
	 * single allocationDetail instance with quantity updated.
	 * 
	 * @param orderLines
	 * @param firstOrderLine
	 */
	private void consolidateAllocationDetails(List<OrderLine> orderLines, OrderLine firstOrderLine) {

		List<AllocationDetail> allocationDetails = orderLines.stream()
				.map(orderLine -> orderLine.getAllocationDetails()).filter(PredicateManager.getNotNullFilter())
				.map(allocation -> allocation.getAllocationDetail()).filter(PredicateManager.getNotNullFilter())
				.flatMap(allocationDetail -> allocationDetail.stream()).collect(Collectors.toList());
		//merge(allocationDetails,firstOrderLine);
		merge(allocationDetails,firstOrderLine);
	}

	private void merge(List<AllocationDetail> allocationDetails, OrderLine firstOrderLine) {
		//grouped AllocationDetail (of all same orderlines based on the facilityId. 
		//If the allocation is from the same facility, aggregate the quantity and other details.
		Map<String,List<AllocationDetail>> groupedAllocations = 
				allocationDetails.stream()
				.filter(PredicateManager.getNotNullFilter())
				.collect(Collectors.groupingBy(allocationDetail -> allocationDetail.getOriginFacilityAliasID().getValue()));
		
		List<AllocationDetail> allocations = 
				groupedAllocations.entrySet().stream().map(entry -> consolidateAllocationInfo(entry.getValue())).collect(Collectors.toList());
		
		firstOrderLine.getAllocationDetails().getAllocationDetail().clear();
		firstOrderLine.getAllocationDetails().getAllocationDetail().addAll(allocations);
	}
	
	private AllocationDetail consolidateAllocationInfo(List<AllocationDetail> allocations) {
		Optional<AllocationDetail> firstAllocationOptional = allocations.stream().findFirst();
		AllocationDetail firstAllocation  = firstAllocationOptional.get();
		
		BigDecimal allocatedQty = 
				allocations.stream()
				.filter(PredicateManager.getNotNullFilter())
				.map(alloc -> alloc.getAllocatedQuantity().getValue())
				.filter(PredicateManager.getNotEmptyFilter())
		.map(val -> new BigDecimal(val)).reduce(BigDecimal.ZERO, (first,next) -> first.add(next));
		firstAllocation.getAllocatedQuantity().setValue(allocatedQty.toPlainString());
		return firstAllocation;
	}
	
	
	/**
	 * Checks for the deep equality of two AllocationDetail instances
	 * 
	 * @param a
	 * @param b
	 * @return
	 */
	private boolean compare(AllocationDetail a, AllocationDetail b) {
		return Objects.equals(a.getItemID().getValue(), b.getItemID().getValue())
				&& Objects.equals(a.getSupplyID().getValue(), b.getSupplyID().getValue())
				&& Objects.equals(a.getSupplyType(), b.getSupplyType())
				&& Objects.equals(a.getSupplyDetailID().getValue(), b.getSupplyDetailID().getValue())
				&& Objects.equals(a.getIsVirtualAllocation().getValue(), b.getIsVirtualAllocation().getValue())
				&& Objects.equals(a.getSubstitutionRatio().getValue(), b.getSubstitutionRatio().getValue())
				&& Objects.equals(a.getTierId().getValue(), b.getTierId().getValue())
				&& Objects.equals(a.getOriginFacilityAliasID().getValue(), b.getOriginFacilityAliasID().getValue())
				&& Objects.equals(a.getFacilityType().getValue(), b.getFacilityType().getValue())
				&& Objects.equals(a.getAllocationStatus(), b.getAllocationStatus())
				&& Objects.equals(a.getSubstitutionType().getValue(), b.getSubstitutionType().getValue())
				&& Objects.equals(a.getInventorySegmentName().getValue(), b.getInventorySegmentName().getValue());
	}

}
