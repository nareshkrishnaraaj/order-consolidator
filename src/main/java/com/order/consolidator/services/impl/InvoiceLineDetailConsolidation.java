package com.order.consolidator.services.impl;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.order.consolidator.entities.TXML.Message.Order.Invoices.InvoiceDetail.InvoiceLines.InvoiceLineDetail;
import com.order.consolidator.services.AbstractBaseConsolidation;
import com.order.consolidator.services.OrderLineConsolidation;

/**
 * Manages the consolidation process for InvoiceLineDetails
 * 
 * @author Ismail
 *
 */
@Service
public class InvoiceLineDetailConsolidation implements OrderLineConsolidation<InvoiceLineDetail> {

	private static final Logger LOGGER = LogManager.getLogger(InvoiceLineDetailConsolidation.class);

	@Autowired
	InvoiceLineBaseConsolidation baseDetails;

	@Autowired
	InvoiceLineTaxDetailConsolidation taxDetails;

	@Autowired
	InvoiceLineDiscountTotalConsolidation discountDetails;

	@Autowired
	InvoiceLineChargeDetailConsolidation chargeDetails;

	@Override
	public InvoiceLineDetail consolidate(List<InvoiceLineDetail> list) {
		executeConsolidationServices(list);
		dropAdditionalOrderLines(list);
		Optional<InvoiceLineDetail> optional = list.stream().findFirst();
		return optional.get();
	}

	/**
	 * Invokes the defined consolidation services for the given InvoiceLineDetail.
	 * 
	 * @param orderLines
	 */
	private void executeConsolidationServices(List<InvoiceLineDetail> list) {
		LOGGER.info("Invoking the consolidation services.");

		// commenting below code. If the consolidation logic is skipped (when there's
		// only 1 invoiceLinedetail), referenceField15 is not getting updated across the
		// chargeDetails, discountDetails.
		/*
		 * if(list.size()<2) { return; }
		 */
		// Performs the consolidation and updates the first InvoiceLineDetail
		List<AbstractBaseConsolidation<InvoiceLineDetail>> consolidatonServices = Arrays.asList(baseDetails, taxDetails,
				chargeDetails, discountDetails);
		consolidatonServices.stream().forEach(service -> service.performConsolidation(list));

		LOGGER.info("Completed the consolidation services for InvoiceLines");
	}

	/**
	 * Drops the other duplicate orderlines and updates the quantity accordingly
	 * 
	 * @param orderLines
	 */
	private void dropAdditionalOrderLines(List<InvoiceLineDetail> invoiceLines) {
		Optional<InvoiceLineDetail> optional = invoiceLines.stream().findFirst();
		if (!optional.isPresent())
			return;
		invoiceLines.removeIf(o -> !o.equals(optional.get()));
		LOGGER.info("Dropped additional invoiceLines.");
	}
}
