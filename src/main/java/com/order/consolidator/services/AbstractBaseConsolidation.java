package com.order.consolidator.services;

import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Service;
import com.order.consolidator.entities.TXML.Message.Order.OrderLines.OrderLine;

/**
 * Base class for customizing the consolidation mechanism.
 * @author Ismail
 *
 */
@Service
public abstract class AbstractBaseConsolidation<T> {

	/**
	 * Validate the basic requried fields to perform the consolidation process. The
	 * Concrete class must override the method if there are other attributes that
	 * are specific to the type of consolidation.
	 * 
	 * @param orderLines
	 * @return
	 */
	
	/*
	protected boolean validateRequiredFields(List<OrderLine> orderLines) {
		Optional<OrderLine> optional = orderLines.stream().findFirst();
		if (optional.isPresent())
			return Boolean.TRUE;

		return Boolean.FALSE;

	}
*/
	
	protected boolean validateRequiredFields(List<T> collection) {
		Optional<T> optional = collection.stream().findFirst();
		if (optional.isPresent())
			return Boolean.TRUE;

		return Boolean.FALSE;
	}
	
	/**
	 * Performs the consolidation of amounts based on the type of consolidation
	 * process defined by the Concrete class. There is no default implementation.
	 * 
	 * @param orderLine
	 */
	public abstract void performConsolidation(List<T> t);

}
