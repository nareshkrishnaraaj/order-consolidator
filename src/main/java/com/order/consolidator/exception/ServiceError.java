package com.order.consolidator.exception;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.http.HttpStatus;

/**
 * POJO for constructing the error response
 * @author Ismail
 *
 */
@XmlRootElement
//@XmlType (propOrder={"errorCode", "errorMessage" , "errorDescription" })
public class ServiceError {

	
	private HttpStatus errorCode;
	private List<String> errorMessage;
	private String errorDescription;
	
	public String getErrorDescription() {
		return errorDescription;
	}

	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	} 

	public ServiceError(){
		
	}
	
	public ServiceError(List<String> errorMessages) {
		this.setErrorMessages(errorMessages);
	}

	public List<String> getErrorMessages() {
		return errorMessage;
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessage = errorMessages;
	}
	
	public HttpStatus getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(HttpStatus errorCode) {
		this.errorCode = errorCode;
	}
}
