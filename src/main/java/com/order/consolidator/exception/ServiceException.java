package com.order.consolidator.exception;

import java.util.Arrays;
import javax.xml.bind.JAXBException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.xml.sax.SAXParseException;

import com.order.consolidator.util.Constants;

/**
 * Custom Exception for client response.
 * @author Ismail
 *
 */
public class ServiceException extends RuntimeException {
	
	private static final Logger LOGGER = LogManager.getLogger(ServiceException.class);

	private static final long serialVersionUID = 1L;
	
	
	private ServiceError serviceError;
	
	public ServiceException(Exception e) {
		initializeErrorMessages(e);
	}
	
	public ServiceException(String detailMessage) {
		super(detailMessage);
	}

	public ServiceError getErrorMessages() {
		return serviceError;
	}

	
	public void setErrorMessages(ServiceError serviceError) {
		this.serviceError = serviceError;
	}
	
	private void initializeErrorMessages(Exception e) {
		this.serviceError = new ServiceError();

		if(e instanceof SAXParseException) {
			SAXParseException se = (SAXParseException)e;
			this.serviceError.setErrorMessages(Arrays.asList(Constants.BAD_REQUEST_ERROR));
			this.serviceError.setErrorCode(HttpStatus.BAD_REQUEST);
			if(se.getMessage() !=null) {
				this.serviceError.setErrorDescription("Line: "+se.getLineNumber()+
						", Column: "+se.getColumnNumber()+". "+se.getMessage());
				LOGGER.error("se.getMessage():"+se.getMessage()); 
			}
		} else if(e instanceof JAXBException) {
			this.serviceError.setErrorMessages(Arrays.asList(Constants.BAD_REQUEST_ERROR));
			this.serviceError.setErrorCode(HttpStatus.BAD_REQUEST);
			if(e.getMessage() !=null) {
				this.serviceError.setErrorDescription(e.getCause().getMessage());
			}
		} else {
			this.serviceError.setErrorMessages(Arrays.asList(Constants.INTERNAL_ERROR));
			this.serviceError.setErrorCode(HttpStatus.INTERNAL_SERVER_ERROR);
			if(e.getMessage() != null) {
				this.serviceError.setErrorDescription(e.getMessage());
				LOGGER.error(e.getMessage());
			} else if(e.getStackTrace()!=null && e.getStackTrace().length>0) {
				this.serviceError.setErrorDescription(e.getStackTrace()[0].toString());
			}
		}
	}
}
