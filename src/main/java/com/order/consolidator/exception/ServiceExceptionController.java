package com.order.consolidator.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.util.HtmlUtils;

/**
 * Catches all Runtime Exceptions of type ServiceException and constructs the response accordingly.
 * @author Ismail
 *
 */
@ControllerAdvice
public class ServiceExceptionController {
	
	@ExceptionHandler
	public ResponseEntity<Object> exception(ServiceException e){
		
		ServiceError error = new ServiceError();
		error.setErrorCode(e.getErrorMessages().getErrorCode());
		error.setErrorMessages(e.getErrorMessages().getErrorMessages());
		if(!StringUtils.isEmpty(e.getErrorMessages().getErrorDescription())){
			String desc =e.getErrorMessages().getErrorDescription();
			//Escaping the special xml characters due to which error response was not logged in BEF.
			if(e.getErrorMessages().getErrorDescription().indexOf("<") > 0 || e.getErrorMessages().getErrorDescription().indexOf(">") > 0) {
				String lt = HtmlUtils.htmlEscape("<");
				String gt = HtmlUtils.htmlEscape(">");
				desc = e.getErrorMessages().getErrorDescription().replaceAll("<", lt);
				desc = desc.replaceAll(">", gt);
			}
				error.setErrorDescription(desc);
		}
		return new ResponseEntity<>(error,error.getErrorCode());
	}
}
